/**
 * 
 */
package com.xiaoyun.org.app.common;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * 调用Android系统的分享功能
 * 
 * @author yuanxy
 * 
 */
public class SystemShare {

	/**
	 * 纯文本的分享
	 * 
	 * @param context
	 * @param shareMsg
	 *            分享内容
	 */
	public static void shareMsg(Context context, String shareMsg) {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, shareMsg);
		sendIntent.setType("text/plain");
		context.startActivity(Intent.createChooser(sendIntent, "分享"));
	}

	/**
	 * 带图片的分享
	 * 
	 * @param context
	 * @param shareMsg
	 *            分享内容
	 * @param imgPath
	 *            sd卡图片路径
	 */
	public static void shareMsg(Activity context, String shareMsg,
			String imgPath) {
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		if (imgPath == null || imgPath.equals("")) {
			sendIntent.setType("text/plain");
		} else {
			File f = new File(imgPath);
			if (f != null && f.exists() && f.isFile()) {
				sendIntent.setType("image/jpg");
				Uri u = Uri.fromFile(f);
				sendIntent.putExtra(Intent.EXTRA_STREAM, u);
			}
		}
		sendIntent.putExtra(Intent.EXTRA_TEXT, shareMsg);
		sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(Intent.createChooser(sendIntent, "分享"));
	}

}
